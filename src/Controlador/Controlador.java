/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Controlador;

import Modelos.*;
import Vistas.*;
import java.awt.Color;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;

/**
 *
 * @author Brandon
 */
public class Controlador implements ActionListener {

    dbPreguntas dbQuiz;
    dlgLogin login;
    dlgRegistro registro;
    dlgContra contra;
    dlgHome home;
    dlgInicio inicio;
    dlgRanking ranking;
    dlgCorrecto correcto;
    dlgError error;
    Preguntas preguntas;
    Quiz quiz;

    //private int puntuacion = 0;
    private int vidas = 3;
    private String Correcta = "";

    public Controlador(dbPreguntas dbQuiz, dlgLogin login, dlgRegistro registro, dlgContra contra, dlgHome home, dlgInicio inicio, dlgRanking ranking, dlgCorrecto correcto, dlgError error, Preguntas preguntas, Quiz quiz) {
        this.dbQuiz = dbQuiz;
        this.login = login;
        this.registro = registro;
        this.contra = contra;
        this.home = home;
        this.inicio = inicio;
        this.ranking = ranking;
        this.correcto = correcto;
        this.error = error;
        this.preguntas = preguntas;

        this.quiz = quiz;

        login.btnCambioContra.addActionListener(this);
        login.btnIngresar.addActionListener(this);
        login.btnRegistrarse.addActionListener(this);
        registro.btnAceptar.addActionListener(this);
        registro.btnCancelar.addActionListener(this);
        contra.btnAceptar.addActionListener(this);
        contra.btnCancelar.addActionListener(this);
        inicio.btnIniciar.addActionListener(this);

        correcto.btnAvanzar.addActionListener(this);
        correcto.btnSalir.addActionListener(this);
        error.btnAvanzar.addActionListener(this);
        error.btnSalir.addActionListener(this);

        home.btnOp1.addActionListener(this);
        home.btnOp2.addActionListener(this);
        home.btnOp3.addActionListener(this);
        //cerrar
        inicio.btnCerrar.addActionListener(this);
        home.btnCerrar.addActionListener(this);
        ranking.btnCerrar.addActionListener(this);
        login.btnCerrar.addActionListener(this);

    }
    private boolean stopBar = false;
    private int tiempo = 0;

    private void iniLogin() {
        login.setSize(1000, 1000);
        login.setVisible(true);
    }

    private void iniContra() {
        contra.setSize(500, 500);
        contra.setLocationRelativeTo(login);
        contra.setVisible(true);
    }

    private void iniRegistro() {
        registro.setSize(500, 500);
        registro.setLocationRelativeTo(login);
        registro.setVisible(true);
    }

    private void iniInicio() {
        inicio.setSize(1000, 1000);
        inicio.setVisible(true);
    }

    private void iniCorrecto() {
        correcto.setSize(500, 500);
        correcto.setLocationRelativeTo(home);
        correcto.setVisible(true);
    }

    private void iniError() {
        error.setSize(500, 500);
        error.setLocationRelativeTo(home);
        error.setVisible(true);
    }

    private void iniRanking() {
        ranking.setSize(1000, 1000);
        //regRanking();
        ranking.setVisible(true);
    }

    private void mostrarRanking() {
        ArrayList<Quiz> lista = new ArrayList<>();
        try {
            lista = dbQuiz.listarUsuario();

            //Datos Jugador
            ranking.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource(setAvatarRanking(quiz.getAvatar()))));
            ranking.lbUsuario.setText(quiz.getUsuario());
            ranking.lbNivel.setText(Integer.toString(quiz.getPuntuacion()));
            ranking.lbVidas.setIcon(new javax.swing.ImageIcon(getClass().getResource(vidas())));

            //Usuario 1
            Quiz user1 = lista.get(0);
            //Imagen
            ranking.lbAvatar1.setIcon(new javax.swing.ImageIcon(getClass().getResource(setAvatarRanking(user1.getAvatar()))));
            //Nombre
            ranking.lbUsuario1.setText(user1.getUsuario());
            //Puntuacion
            ranking.lbNivel1.setText(Integer.toString(user1.getPuntuacion()));

            //Usuario 2
            Quiz user2 = lista.get(1);
            //Imagen
            ranking.lbAvatar2.setIcon(new javax.swing.ImageIcon(getClass().getResource(setAvatarRanking(user2.getAvatar()))));
            //Nombre
            ranking.lbUsuario2.setText(user2.getUsuario());
            //Puntuacion
            ranking.lbNivel2.setText(Integer.toString(user2.getPuntuacion()));

            //Usuario 3
            Quiz user3 = lista.get(2);
            //Imagen
            ranking.lbAvatar3.setIcon(new javax.swing.ImageIcon(getClass().getResource(setAvatarRanking(user3.getAvatar()))));
            //Nombre
            ranking.lbUsuario3.setText(user3.getUsuario());
            //Puntuacion
            ranking.lbNivel3.setText(Integer.toString(user3.getPuntuacion()));

            iniRanking();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(ranking, "Algo salio mal: " + ex);
        }
    }

    private String setAvatarRanking(int a) {
        String ruta = "";
        switch (a) {
            case 1:
                ruta = "/imagenes/11.png";
                break;
            case 2:
                ruta = "/imagenes/12.png";
                break;
            case 3:
                ruta = "/imagenes/13.png";
                break;
            case 4:
                ruta = "/imagenes/14.png";
                break;
        }

        return ruta;
    }

    private String vidas() {
        String ruta = "";

        switch (this.vidas) {
            case 0:
                ruta = "/imagenes/0 vidas.png";
                break;
            case 1:
                ruta = "/imagenes/1 vidas.png";
                break;
            case 2:
                ruta = "/imagenes/2vidas.png";
                break;
            default:
                ruta = "/imagenes/3vidas.png";
        }
        return ruta;
    }

    private void iniHome() {
        home.setSize(1000, 1000);
        home.pbTiempo.setValue(0); // Inicializa el valor del progreso a 0
        // Inicia el hilo para incrementar el progreso
        setAvatarHome(quiz.getAvatar());
        home.lbUsuario.setText(quiz.getUsuario());
        startBar();
        home.setVisible(true);
    }

    //Asigna valor al setAvatar.......
    private void avatar() {
        Random random = new Random();
        quiz.setAvatar(random.nextInt(4) + 1);
    }

    //Recibe el getAvatar y cambia el Icon del HOME......
    private void setAvatarHome(int a) {
        switch (a) {
            case 1:
                home.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/11.png")));
                break;
            case 2:
                home.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/12.png")));
                break;
            case 3:
                ranking.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/11.png")));
                break;
            case 4:
                ranking.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/11.png")));
                break;
        }
    }
    //Recibe el getAvatar y cambia el Icon del Inicio......

    private void setAvatarInicio(int a) {
        switch (a) {
            case 1:
                inicio.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Avatar 01.png")));
                break;
            case 2:
                inicio.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Avatar 02.png")));
                break;
            case 3:
                inicio.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Avatar 03.png")));
                break;
            case 4:
                inicio.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Avatar 04.png")));
                break;
        }

    }

    private void regPregunta(int nivel) {
        ArrayList<Preguntas> listas = new ArrayList<>();
        try {
            listas = dbQuiz.generar(nivel); //pasamos las preguntas---
            //En generar, hice unos cambios, para que solo regrese preguntas con un cierto nivel.

            Random random = new Random();
            int pr = 0;
            if (!listas.isEmpty()) {
                pr = (random.nextInt(listas.size() - 1) + 1);
               /* System.out.println("El valor de pr1" + listas.size());*/
                Preguntas pregunta = listas.get(pr); // un numero al azar 

                this.Correcta = pregunta.getOp1();

                ArrayList<String> opciones = new ArrayList<>();
                opciones.add(pregunta.getOp1());
                opciones.add(pregunta.getOp2());
                opciones.add(pregunta.getOp3());

                // Mezclar aleatoriamente las opciones
                Collections.shuffle(opciones);

                // Asignar las opciones mezcladas a los botones
                home.lbPregunta.setText(pregunta.getPregunta());
                home.btnOp1.setText(opciones.get(0));
                home.btnOp2.setText(opciones.get(1));
                home.btnOp3.setText(opciones.get(2));

                /*home.lbPregunta.setText(pregunta.getPregunta());
                home.btnOp1.setText(pregunta.getOp1());
                home.btnOp2.setText(pregunta.getOp2());
                home.btnOp3.setText(pregunta.getOp3());*/
                //home.lbNivel.setText(nivel + "/10");
               /* System.out.println("El valor de pr " + pr);*/
            } else {
                System.out.println("No hay preguntas con ese nivel");
            }
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(ranking, "Fallo sss: " + ex);
        } catch (Exception ex2) {
            JOptionPane.showMessageDialog(ranking, "Algo salio mal sss: " + ex2);
        }
    }

    private boolean isCorrecto(String op) {
        boolean res = false;

        if (this.Correcta == op) {
            res = true;
        } else {
            res = false;
        }
        return res;
    }

    private void bien() {
        quiz.puntuacion(tiempo, preguntas.getNivel());

        try {
            dbQuiz.puntuacion(quiz);
        } catch (Exception ex) {
            Logger.getLogger(Controlador.class.getName()).log(Level.SEVERE, null, ex);
        }

        iniCorrecto();
    }
    
    private void mal(){
         iniError();
    }

    private void startBar() {
        regPregunta(preguntas.getNivel()); //genera la pregunta
        SwingWorker<Void, Integer> worker = new SwingWorker<Void, Integer>() {
            @Override
            protected Void doInBackground() throws Exception {
                for (tiempo = 0; tiempo <= 100 && !stopBar; tiempo++) {
                    Thread.sleep(100);
                    publish(tiempo); // Publica el valor del progreso
                }
                return null;
            }

            @Override
            protected void process(java.util.List<Integer> chunks) {
                int progressValue = chunks.get(chunks.size() - 1);
                home.pbTiempo.setValue(progressValue); // Actualiza el valor del progreso en la GUI
                if (home.pbTiempo.getValue() == 100) { //si se acaba el tiempo
                    JOptionPane.showMessageDialog(home, "UPS, eres muy lento");
                    iniError(); //manda a llamar dlgError
                }
                /*if (preguntas.getNivel() > 10) {
                    StopBar(); //Parar la barra 
                    JOptionPane.showMessageDialog(home, "Tu puntuacion es de " + quiz.getPuntuacion());
                    //JOptionPane.showMessageDialog(home, "Se acabaron las preguntas");
                    home.setVisible(false);
                    mostrarRanking();
                }*/
            }

            @Override
            protected void done() {
                // Tareas después de completar el progreso
            }
        };

        worker.execute();
    }

    private void StopBar() {
        stopBar = true;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        dbPreguntas dbQuiz = new dbPreguntas();
        dlgLogin login = new dlgLogin(new JFrame(), true);
        dlgRegistro registro = new dlgRegistro(new JFrame(), true);
        dlgContra contra = new dlgContra(new JFrame(), true);
        dlgHome home = new dlgHome(new JFrame(), true);
        dlgInicio inicio = new dlgInicio(new JFrame(), true);
        dlgRanking ranking = new dlgRanking(new Frame(), true);
        dlgCorrecto correcto = new dlgCorrecto(new Frame(), true);
        dlgError error = new dlgError(new Frame(), true);
        Preguntas preguntas = new Preguntas();
        Quiz quiz = new Quiz();
        Controlador con = new Controlador(dbQuiz, login, registro, contra, home, inicio, ranking, correcto, error, preguntas, quiz);

        con.iniLogin();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == login.btnIngresar) {
            try {

                quiz.setUsuario(login.txtUsuario.getText());
                quiz.setContra(login.txtContra.getText());
                login.txtUsuario.setText("");
                login.txtContra.setText("");
                if (dbQuiz.isCoinciden(quiz.getUsuario(), quiz.getContra())) {
                    quiz = (Quiz) dbQuiz.buscar(quiz.getUsuario());
                    //quiz = quiz;
                    //dbQuiz.buscar(quiz.getUsuario());
                    login.setVisible(false);
                    inicio.lbUsuario.setText(quiz.getUsuario());
                    preguntas.setNivel(1);
                    setAvatarInicio(quiz.getAvatar());
                    iniInicio();

                } else {
                    JOptionPane.showMessageDialog(login, "Usuario o contraseña incorrecto");
                }
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(login, "-Error-" + ex2);
            }
        }
        if (e.getSource() == login.btnRegistrarse) {
            iniRegistro();
        }
        if (e.getSource() == login.btnCambioContra) {
            iniContra();
        }
        if (e.getSource() == contra.btnAceptar) {
            try {
                if (dbQuiz.isExiste(contra.txtUsuario.getText())) {
                    if (contra.txtContraNueva.getText().equals(contra.txtContraConfirma.getText()) && !contra.txtContraNueva.getText().equals("")) {
                        quiz.setUsuario(contra.txtUsuario.getText());
                        quiz.setContra(contra.txtContraNueva.getText());
                        dbQuiz.actualizar(quiz);
                        JOptionPane.showMessageDialog(contra, "Se cambio la contraseña con exito");
                        contra.txtContraConfirma.setText("");
                        contra.txtContraNueva.setText("");
                        contra.txtUsuario.setText("");
                        contra.setVisible(false);
                    } else {
                        JOptionPane.showMessageDialog(contra, "Las contraseñas no coinciden...");
                    }
                } else {
                    JOptionPane.showMessageDialog(contra, "El usuario no existe");
                }
            } catch (NullPointerException ex) {
                JOptionPane.showMessageDialog(contra, "-Error-", "Llena todos los campos", JOptionPane.WARNING_MESSAGE);
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(contra, "-Error-", "Algo salio mal", JOptionPane.WARNING_MESSAGE);
            }
        }
        if (e.getSource() == contra.btnCancelar) {
            contra.txtContraConfirma.setText("");
            contra.txtContraConfirma.setText("");
            contra.txtUsuario.setText("");
            contra.setVisible(false);
        }
        if (e.getSource() == registro.btnAceptar) {
            try {
                if (!dbQuiz.isExiste(registro.txtUsuario.getText())) {
                    if (registro.txtContra.getText().equals(registro.txtContraConfirma.getText()) && !registro.txtContra.getText().equals("")) {
                        quiz.setUsuario(registro.txtUsuario.getText());
                        quiz.setContra(registro.txtContra.getText());
                        avatar();
                        quiz.setPuntuacion(0);
                        dbQuiz.registrar(quiz);
                        registro.txtContra.setText("");
                        registro.txtContraConfirma.setText("");
                        registro.txtUsuario.setText("");
                        JOptionPane.showMessageDialog(registro, "Te haz registrado con exito");
                        registro.setVisible(false);
                    } else {
                        JOptionPane.showMessageDialog(registro, "Las contraseñas no coinciden o no son validas...");
                    }
                } else {
                    JOptionPane.showMessageDialog(registro, "El usuario YA existe");
                    registro.txtContra.setText("");
                    registro.txtContraConfirma.setText("");
                    registro.txtUsuario.setText("");
                }
            } catch (NullPointerException ex) {
                JOptionPane.showMessageDialog(registro, "-Error-", "Llena todos los campos", JOptionPane.WARNING_MESSAGE);
            } catch (Exception ex2) {
                JOptionPane.showMessageDialog(registro, "-Error-", "Algo salio mal", JOptionPane.WARNING_MESSAGE);
            }
        }
        if (e.getSource() == registro.btnCancelar) {
            registro.txtContra.setText("");
            registro.txtContraConfirma.setText("");
            registro.txtUsuario.setText("");
            registro.setVisible(false);
        }
        if (e.getSource() == inicio.btnIniciar) {
            inicio.setVisible(false);
            //setAvatarHome(quiz.getAvatar());           
            home.lbAvatar.setIcon(new javax.swing.ImageIcon(getClass().getResource(setAvatarRanking(quiz.getAvatar()))));
            home.lbUsuario.setText(quiz.getUsuario());
            quiz.setPuntuacion(0);
            home.lbNivel.setText(Integer.toString(quiz.getPuntuacion()));

            iniHome();

        }
        if (e.getSource() == home.btnOp1) {
            StopBar();

            if (this.isCorrecto(home.btnOp1.getText()) == true) {
                bien();
            } else {
                mal();
            }
        }
        if (e.getSource() == home.btnOp2) {
            StopBar();
           if (this.isCorrecto(home.btnOp2.getText()) == true) {
                bien();
            } else {
                mal();
            }
        }
        if (e.getSource() == home.btnOp3) {
            StopBar();
            if (this.isCorrecto(home.btnOp3.getText()) == true) {
                bien();
            } else {
                mal();
            }
        }
        if (e.getSource() == correcto.btnAvanzar) {

            home.lbNivel.setText(Integer.toString(quiz.getPuntuacion()));

            preguntas.setNivel(preguntas.getNivel() + 1);
            stopBar = false;
            startBar();
            correcto.setVisible(false);

        }
        if (e.getSource() == error.btnAvanzar) {
            this.vidas -= 1;

            if (this.vidas > 0) {
                home.lbVidas.setIcon(new javax.swing.ImageIcon(getClass().getResource(vidas())));
            } else {
                mostrarRanking();
            }

            preguntas.setNivel(preguntas.getNivel() + 1);
            stopBar = false;
            startBar();
            error.setVisible(false);
        }
        if (e.getSource() == correcto.btnSalir) {
            correcto.setVisible(false);
            home.setVisible(false);
            mostrarRanking();
        }
        if (e.getSource() == error.btnSalir) {
            this.vidas -= 1;

            if (this.vidas > 0) {
                home.lbVidas.setIcon(new javax.swing.ImageIcon(getClass().getResource(vidas())));
            } else {
                mostrarRanking();
            }

            error.setVisible(false);
            home.setVisible(false);
            mostrarRanking();
        }
        if (e.getSource() == inicio.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(inicio, "¿Deseas regresar al login?",
                    "Regresar", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                inicio.setVisible(false);
                iniLogin();
            }
        }
        if (e.getSource() == home.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(home, "¿Deseas terminar la partida?",
                    "Cerrar", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                home.setVisible(false);
                StopBar();
                mostrarRanking();
            }
        }
        if (e.getSource() == ranking.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(ranking, "¿Deseas salir del Ranking?",
                    "Cerrar", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                ranking.setVisible(false);
                /*iniLogin();*/
               System.exit(0);
            }
        }
        if (e.getSource() == login.btnCerrar) {
            int option = JOptionPane.showConfirmDialog(ranking, "¿Deseas salir del Juego?",
                    "Salir", JOptionPane.YES_NO_OPTION);
            if (option == JOptionPane.YES_NO_OPTION) {
                System.exit(0);
            }
        }
    }

}
