/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 10.4.28-MariaDB : Database - quiz
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`quiz` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

USE `quiz`;

/*Table structure for table `preguntas` */

DROP TABLE IF EXISTS `preguntas`;

CREATE TABLE `preguntas` (
  `idPregunta` int(11) NOT NULL AUTO_INCREMENT,
  `pregunta` varchar(250) DEFAULT NULL,
  `r1` varchar(50) DEFAULT NULL,
  `r2` varchar(50) DEFAULT NULL,
  `r3` varchar(50) DEFAULT NULL,
  `nivel` int(11) DEFAULT NULL,
  PRIMARY KEY (`idPregunta`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `preguntas` */

insert  into `preguntas`(`idPregunta`,`pregunta`,`r1`,`r2`,`r3`,`nivel`) values 
(1,'¿Cuál es el planeta más grande del sistema solar?','Júpiter','Saturno','Neptuno',2),
(2,'¿Quién pintó \"La Mona Lisa\"?','Leonardo da Vinci','Pablo Picasso','Michelangelo',1),
(3,'¿Cuál es el metal más abundante en la corteza terrestre?','Aluminio','Hierro','Cobre',2),
(4,'¿En qué año se produjo la Revolución Francesa?','1789','1804','1815',1),
(5,'¿Cuál es el río más largo del mundo?','Nilo','Amazonas','Yangtsé',2),
(6,'¿Cuál es el proceso por el cual los seres vivos toman oxígeno y liberan dióxido de carbono?','Respiración','Fotosíntesis','Digestión',2),
(7,'¿Qué famoso escritor ruso escribió \"Guerra y paz\"?','Lev Tolstói','Fyodor Dostoevsky','Anton Chekhov',1),
(8,'¿Cuál es el océano que baña las costas de Sudáfrica?','Océano Atlántico','Océano Índico','Océano Pacífico',1),
(9,'¿Cuál es el océano más grande del mundo?','Océano Pacífico','Océano Atlántico','Océano Índico',1),
(10,'¿Qué animal es conocido como \"el rey de la selva\"?','León','Tigre','Elefante',1),
(11,'¿Quién escribió \"La metamorfosis\"?','Franz Kafka','Gabriel García Márquez','Fyodor Dostoevsky',2),
(12,'¿Cuál es el proceso por el cual las plantas capturan la luz solar para producir su alimento?','Fotosíntesis','Respiración','Transpiración',1),
(13,'¿En qué continente se encuentra el desierto del Sahara?','África','Asia','América del Norte',1),
(14,'¿Qué científico formuló las leyes del movimiento planetario?','Johannes Kepler','Galileo Galilei','Isaac Newton',2),
(15,'¿Cuál es el órgano encargado de bombear sangre en el cuerpo humano?','Corazón','Pulmón','Riñón',1),
(16,'¿En qué año se llevó a cabo el primer vuelo controlado por los hermanos Wright?','1903','1910','1899',1),
(17,'¿Cuál es el metal más ligero en la tabla periódica?','Hidrógeno','Helio','Litio',1),
(18,'¿Quién fue el autor de \"El retrato de Dorian Gray\"?','Oscar Wilde','Herman Melville','Charles Dickens',2),
(19,'¿Cuál es el animal más grande del planeta Tierra?','Ballena azul','Elefante africano','Tiburón ballena',4),
(20,'¿Qué país tiene forma de bota?','Italia','España','Grecia',3),
(21,'¿Cuál es el símbolo químico del uranio?','U','Un','Ur',4),
(22,'¿En qué año se fundó la Cruz Roja Internacional?','1863','1876','1901',4),
(23,'¿Cuál es el océano que baña las costas de la India?','Océano Índico','Océano Pacífico','Mar Arábigo',4),
(24,'¿En qué año se firmó la Declaración de Derechos Humanos?','1948','1955','1961',4),
(25,'¿Cuál es el proceso por el cual un líquido pasa a estado gaseoso?','Evaporación','Condensación','Fusión',4),
(26,'¿Qué científico desarrolló la teoría de la evolución por selección natural?','Charles Darwin','Gregor Mendel','Albert Einstein',4),
(27,'¿Cuál es el metal utilizado comúnmente en las baterías?','Zinc','Plomo','Litio',4),
(28,'¿Cuál es la capital de Canadá?','Ottawa','Toronto','Vancouver',3),
(29,'¿En qué continente se encuentra la Cordillera de los Andes?','América del Sur','América del Norte','Asia',3),
(30,'¿Quién escribió \"La Ilíada\"?','Homero','Sófocles','Virgilio',3),
(31,'¿Cuál es el océano que baña las costas de China?','Océano Pacífico','Océano Índico','Océano Atlántico',4),
(32,'¿En qué año se produjo la Revolución Industrial?','Siglo XVIII (1700s)','Siglo XIX (1800s)','Siglo XX (1900s)',3),
(33,'¿Qué científico enunció las leyes del movimiento planetario?','Johannes Kepler','Galileo Galilei','Isaac Newton',6),
(34,'¿Cuál es el autor de \"La Odisea\"?','Homero','Sófocles','Virgilio',6),
(35,'¿Qué artista es conocido por pintar \"Los relojes blandos\"?','Salvador Dalí','Pablo Picasso','Claude Monet',6),
(36,'¿Quién fue el primer presidente de Estados Unidos?','George Washington','Thomas Jefferson','Benjamin Franklin',5),
(37,'¿Cuál es el proceso por el cual las plantas liberan agua en forma de vapor?','Transpiración','Evaporación','Condensación',5),
(38,'¿En qué año se produjo la Revolución Mexicana?','1910','1810','1930',6),
(39,'¿Cuál es el elemento químico con el número atómico 79?','Oro (Au)','Plata (Ag)','Cobre (Cu)',5),
(40,'¿Qué científico propuso la teoría de la relatividad?','Albert Einstein','Isaac Newton','Niels Bohr',5),
(41,'¿Cuál es el libro más vendido de todos los tiempos?','La Biblia','El Quijote','Harry Potter y la piedra filosofal',5),
(42,'¿En qué continente se encuentra el río Nilo?','África','Asia','Europa',6),
(43,'¿Quién escribió \"Crimen y castigo\"?','Fiódor Dostoyevski','Lev Tolstói','Anton Chekhov',5),
(44,'¿Cuál es el océano que baña las costas de Argentina?','Océano Atlántico','Océano Pacífico','Mar Caribe',6),
(45,'¿En qué año se fundó la Organización de las Naciones Unidas (ONU)?','1945','1919','1937',6),
(46,'¿Cuál es el río más largo de África?','Nilo','Congo','Zambeze',8),
(47,'¿Quién fue el primer emperador de la dinastía romana?','Augusto (Octavio)','Julio César','Nerón',8),
(48,'¿Cuál es la fórmula química del ácido sulfúrico?','H2SO4','HCl','NaOH',7),
(49,'¿Qué científico descubrió la radioactividad?','Marie Curie','Albert Einstein','Ernest Rutherford',7),
(50,'¿En qué año se produjo la Revolución Bolchevique en Rusia?','1917','1921','1905',8),
(51,'¿Cuál es el proceso de transferencia de calor a través de un material sin movimiento de partículas?','Conducción','Convección','Radiación',8),
(52,'¿Qué famoso dramaturgo escribió \"La tragedia de Hamlet\"?','William Shakespeare','George Bernard Shaw','Anton Chekhov',7),
(53,'¿En qué país se encuentra la ciudad de Petra, famosa por sus construcciones talladas en roca?','Jordania','Egipto','Israel',7),
(54,'¿Cuál es el símbolo químico del plomo?','Pb','L','P',7),
(55,'¿Quién escribió \"En busca del tiempo perdido\"?','Marcel Proust','James Joyce','Franz Kafka',8),
(56,'¿Cuál es el océano que baña las costas de Chile?','Océano Pacífico','Océano Atlántico','Océano Índico',7),
(57,'¿En qué año se produjo la Revolución China que llevó al poder a Mao Zedong?','1949','1955','1945',8),
(58,'¿Cuál es el proceso por el cual los seres vivos obtienen energía de los alimentos en ausencia de oxígeno?','Fermentación','Respiración','Fotosíntesis',8),
(59,'¿Cuál es el símbolo químico del titanio?','Ti','Ta','Tn',7),
(60,'¿Quién fue el autor de \"La guerra y la paz\"?','Lev Tolstói','Fiódor Dostoyevski','Anton Chekhov',7),
(61,'¿Cuál es el océano que baña las costas de Filipinas?','Océano Pacífico','Océano Índico','Océano Atlántico',8),
(62,'¿En qué año se llevó a cabo la Revolución Industrial en Inglaterra?','Siglo XVIII (1700s)','Siglo XIX (1800s)','Siglo XVI (1600s)',8),
(63,'¿Quién fue el pintor del famoso cuadro \"La noche estrellada\"?','Vincent van Gogh','Pablo Picasso','Claude Monet',8),
(64,'¿Cuál es el símbolo químico del mercurio?','Hg','Me','Mc',7),
(65,'¿En qué año se produjo el hundimiento del Lusitania?','1915','1912','1917',7),
(66,'¿Quién fue el filósofo griego discípulo de Sócrates y maestro de Aristóteles?','Platón','Pitágoras','Heráclito',9),
(67,'¿Cuál es el proceso por el cual los núcleos atómicos inestables emiten partículas o radiación para alcanzar un estado más estable?','Desintegración radiactiva','Fusión nuclear','Fisión nuclear',10),
(68,'¿En qué año se estableció la primera Constitución de Estados Unidos?','1787','1776','1791',10),
(69,'¿Cuál es la constante matemática que relaciona la circunferencia de un círculo con su diámetro?','Pi (π)','Epsilon (ε)','Phi (φ)',10),
(70,'¿Quién escribió \"Cien años de soledad\"?','Gabriel García Márquez','Mario Vargas Llosa','Julio Cortázar',10),
(71,'¿Cuál es el elemento químico con el número atómico 92?','Uranio (U)','Neptunio (Np)','Plutonio (Pu)',10),
(72,'¿En qué año se firmó el Tratado de Versalles para poner fin a la Primera Guerra Mundial?','1919','1918','1920',10),
(73,'¿Cuál es el proceso por el cual las partículas sólidas suspendidas en un fluido caen debido a la gravedad?','Sedimentación','Evaporación','Flotación',10),
(74,'¿Qué científico enunció las leyes del movimiento y la ley de la gravitación universal?','Isaac Newton','Albert Einstein','Galileo Galilei',10),
(75,'¿Quién escribió \"El Quijote\"?','Miguel de Cervantes','Federico García Lorca','Jorge Luis Borges',10),
(76,'¿Cuál es el océano más profundo del mundo?','Océano Pacífico','Océano Atlántico','Océano Índico',9),
(77,'¿En qué año se llevó a cabo la Revolución Rusa de Octubre?','1917','1914','1921',9),
(78,'¿Cuál es el proceso por el cual el agua líquida se transforma en hielo?','Solidificación','Fusión','Evaporación',10),
(79,'¿Cuál es el símbolo químico del radón?','Rn','Ra','Rd',9),
(80,'¿Quién fue el autor de \"En el camino\" (\"On the Road\")?','Jack Kerouac','Allen Ginsberg','Hunter S. Thompson',10),
(81,'¿Cuál es el océano que baña las costas de Corea del Sur?','Océano Pacífico','Océano Índico','Océano Atlántico',9),
(82,'¿En qué año se produjo la Revolución Gloriosa en Inglaterra?','1688','1702','1651',9),
(83,'¿Quién fue el arquitecto principal de la Sagrada Familia en Barcelona?','Antoni Gaudí','Frank Lloyd Wright','Le Corbusier',9),
(84,'¿Cuál es el símbolo químico del actinio?','Ac','At','Am',10),
(85,'¿Quién escribió \"La Comedia Divina\" (\"La Divina Comedia\")?','Dante Alighieri','Geoffrey Chaucer','Petrarca',10),
(86,'¿Cuál es el metal líquido a temperatura ambiente?','Mercurio','Plomo','Hierro',1),
(87,'¿Quién escribió \"La isla del tesoro\"?','Robert Louis Stevenson','Mark Twain','Jules Verne',1),
(88,'¿Cuál es el animal terrestre más rápido?','Guepardo','León','Elefante',2),
(89,'¿Qué planeta es conocido como el \"Planeta Rojo\"?','Marte','Júpiter','Venus',1),
(90,'¿Cuál es el proceso por el cual las plantas producen su propio alimento usando la luz solar?','Fotosíntesis','Respiración','Transpiración',1),
(91,'¿Cuál es el símbolo químico del nitrógeno?','N','Ni','Na',4),
(92,'¿Quién escribió \"Cumbres Borrascosas\"?','Emily Brontë','Jane Austen','Charlotte Brontë',4),
(93,'¿Cuál es el océano más grande del mundo en términos de área?','Océano Pacífico','Océano Atlántico','Océano Índico',3),
(94,'¿En qué año se produjo la Revolución Industrial en Inglaterra?','Siglo XVIII (1700s)','Siglo XIX (1800s)','Siglo XVI (1600s)',4),
(95,'¿Cuál es el proceso por el cual los seres vivos toman oxígeno y liberan dióxido de carbono?','Respiración','Fotosíntesis','Digestión',4),
(96,'¿En qué año se produjo la Revolución Mexicana?','1910','1810','1930',6),
(97,'¿Cuál es el proceso por el cual un líquido pasa a estado gaseoso?','Evaporación','Condensación','Fusión',6),
(98,'¿Qué científico descubrió el principio de la conservación de la energía?','Hermann von Helmholtz','Albert Einstein','Niels Bohr',6),
(99,'¿Cuál es el océano que baña las costas de Australia?','Océano Pacífico','Océano Atlántico','Océano Índico',6),
(100,'¿Quién fue el autor de \"Las aventuras de Tom Sawyer\"?','Mark Twain','Charles Dickens','Herman Melville',6),
(101,'¿Cuál es el órgano encargado de filtrar la sangre en el cuerpo humano?','Riñón','Hígado','Pulmón',8),
(102,'¿Quién fue el autor de \"La Odisea\"?','Homero','Sófocles','Virgilio',8),
(103,'¿Cuál es el símbolo químico del platino?','Pt','Pl','Pn',7),
(104,'¿En qué año se fundó la Organización de las Naciones Unidas (ONU)?','1945','1919','1937',8),
(105,'¿Cuál es el proceso por el cual las partículas sólidas suspendidas en un fluido caen debido a la gravedad?','Sedimentación','Evaporación','Flotación',7),
(106,'¿Quién escribió \"La República\"?','Platón','Aristóteles','Sócrates',10),
(107,'¿Cuál es el proceso por el cual se convierte la energía nuclear en energía térmica?','Fisión nuclear','Fusión nuclear','Radiación',9),
(108,'¿En qué año se produjo la Revolución Rusa de Octubre?','1917','1914','1921',9),
(109,'¿Cuál es el símbolo químico del tungsteno?','W','T','Tu',9),
(110,'¿Quién escribió \"La Ilíada\"?','Homero','Sófocles','Virgilio',10);

/*Table structure for table `ranking` */

DROP TABLE IF EXISTS `ranking`;

CREATE TABLE `ranking` (
  `idRanking` int(11) NOT NULL AUTO_INCREMENT,
  `idUsuario` int(11) DEFAULT NULL,
  `puntuacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idRanking`),
  KEY `idUsuario` (`idUsuario`),
  CONSTRAINT `ranking_ibfk_1` FOREIGN KEY (`idUsuario`) REFERENCES `usuarios` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `ranking` */

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(50) NOT NULL,
  `contra` varchar(50) NOT NULL,
  `avatar` varchar(50) DEFAULT NULL,
  `puntuacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `usuarios` */

insert  into `usuarios`(`idUsuario`,`usuario`,`contra`,`avatar`,`puntuacion`) values 
(3,'angel','cisco123','1',6),
(5,'uaua','123','4',18),
(6,'jose','125','3',0),
(13,'Quezada','123','1',0),
(14,'Brandom','123','2',0),
(15,'qwerty','321','4',36);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
