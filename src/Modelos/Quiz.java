package Modelos;

/**
 *
 * @author Angel
 */

/*15 - 11 -> 3
10 - 6 -> 2
5 - 1 -> 1 */

public class Quiz {
    private String usuario;
    private String contra;
    private int avatar;
    private int puntuacion;
    private int id;

    public Quiz() {
    }

    public Quiz(String usuario, String contra, int avatar, int puntuacion, int id) {
        this.usuario = usuario;
        this.contra = contra;
        this.avatar = avatar;
        this.puntuacion = puntuacion;
        this.id = id;
    }
    
   public Quiz(Quiz quiz) {
        this.usuario = quiz.usuario;
        this.contra = quiz.contra;
        this.avatar = quiz.avatar;
        this.puntuacion =quiz.puntuacion;
        this.id = quiz.id;
    } 

    public String getUsuario() {
        return usuario;
    }

    public String getContra() {
        return contra;
    }

    public int getAvatar() {
        return avatar;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public int getId() {
        return id;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setContra(String contra) {
        this.contra = contra;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    public void setId(int id) {
        this.id = id;
    }
       
   public void avatar(){
        int numero = (int)(Math.random()*5+1);
        
        this.setAvatar(numero);
   }    
   
    public void puntuacion(int tiempo, int nivel) {
        if (nivel == 1 || nivel == 2) {
            if (tiempo >= 16) {
                this.puntuacion += 3;
            } else if (tiempo >= 11 && tiempo <= 15) {
                this.puntuacion += 2;
            } else if (tiempo >= 1 && tiempo <= 10) {
                this.puntuacion += 1;
            }
        } else if (nivel == 3 || nivel == 4) {
            if (tiempo >= 11) {
                this.puntuacion += 3;
            } else if (tiempo >= 6 && tiempo <= 10) {
                this.puntuacion += 2;
            } else if (tiempo >= 1 && tiempo <=5) {
                this.puntuacion += 1;
            }
        } else if (nivel == 5) {
             if (tiempo >= 8) {
                this.puntuacion += 3;
            } else if (tiempo >= 5 && tiempo <= 7) {
                this.puntuacion += 2;
            } else if (tiempo >= 1 && tiempo <=4) {
                this.puntuacion += 1;
            }
        }
    }
}
