/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelos;

import java.sql.*;

/**
 *
 * @author Angel
 */
public abstract class dbManejador {
    protected Connection conexion;
    protected PreparedStatement sqlConsulta;
    protected ResultSet registros;
    private String usuario;
    private String database;
    private String contraseña;
    private String driver;
    private String url;

    public dbManejador(Connection conexion, PreparedStatement sqlConsulta, ResultSet registros, String usuario, String database, String contraseña, String driver, String url) {
        this.conexion = conexion;
        this.sqlConsulta = sqlConsulta;
        this.registros = registros;
        this.usuario = usuario;
        this.database = database;
        this.contraseña = contraseña;
        this.driver = driver;
        this.url = url;
    }

    public dbManejador() {
        this.driver = "com.mysql.cj.jdbc.Driver";
        this.database = "quiz";
        this.usuario = "root";
        this.contraseña = "";
        this.url = "jdbc:mysql://localhost:3306/quiz";
        isDriver();
    }

    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }

    public PreparedStatement getSqlConsulta() {
        return sqlConsulta;
    }

    public void setSqlConsulta(PreparedStatement sqlConsulta) {
        this.sqlConsulta = sqlConsulta;
    }

    public ResultSet getRegistros() {
        return registros;
    }

    public void setRegistros(ResultSet registros) {
        this.registros = registros;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public boolean isDriver(){
        boolean exito=false;
        
        try{
            Class.forName(driver);
        }
        catch(ClassNotFoundException e){
            exito=false;
            System.err.println("Surgio un error " + e.getMessage());
            System.exit(-1);
        }
        
        return exito;
    }

    public boolean Conectar(){
        boolean exito = false;
        try{
            this.setConexion(DriverManager.getConnection(this.url,this.usuario,this.contraseña));
            exito=true;
        }
        catch(SQLException e){
            exito=false;
            System.err.println("Surgio un error al conectar" + e.getMessage());
        }
        return exito;
    }
    
    public void Desconectar(){
        try{
            if(!this.conexion.isClosed()) this.getConexion().close();
        }
        catch(SQLException e){
            System.err.println("Error, no fue posible cerrar la conexion " + e.getMessage());
        }
    }
}
