package Modelos;

/**
 *
 * @author Angel
 */
public class Preguntas {

    private int id;
    private String pregunta;
    private String op1;
    private String op2;
    private String op3;
    private int nivel;

    public Preguntas() {
    }

    public Preguntas(int id, String pregunta, String op1, String op2, String op3, int nivel) {
        this.id = id;
        this.pregunta = pregunta;
        this.op1 = op1;
        this.op2 = op2;
        this.op3 = op3;
        this.nivel = nivel;
    }

    public Preguntas(Preguntas preguntas) {
        this.id = preguntas.id;
        this.pregunta = preguntas.pregunta;
        this.op1 = preguntas.op1;
        this.op2 = preguntas.op2;
        this.op3 = preguntas.op3;
        this.nivel = preguntas.nivel;
    }

    public int getId() {
        return id;
    }

    public String getPregunta() {
        return pregunta;
    }

    public String getOp1() {
        return op1;
    }

    public String getOp2() {
        return op2;
    }

    public String getOp3() {
        return op3;
    }

    public int getNivel() {
        return nivel;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public void setOp1(String op1) {
        this.op1 = op1;
    }

    public void setOp2(String op2) {
        this.op2 = op2;
    }

    public void setOp3(String op3) {
        this.op3 = op3;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }   
}
