package Modelos;

/**
 *
 * @author Angel
 */
public class Ranking {
    private int idRanking;
    private String usuario;
    private int puntuacion; 

    public Ranking() {
    }

    public Ranking(int idRanking, String usuario, int puntuacion) {
        this.idRanking = idRanking;
        this.usuario = usuario;
        this.puntuacion = puntuacion;
    }
    
    public Ranking(Ranking ranking) {
        this.idRanking = ranking.idRanking;
        this.usuario = ranking.usuario;
        this.puntuacion = ranking.puntuacion;
    }

    public int getIdRanking() {
        return idRanking;
    }

    public String getUsuario() {
        return usuario;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setIdRanking(int idRanking) {
        this.idRanking = idRanking;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }   
}
