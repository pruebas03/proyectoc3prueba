package Modelos;

/**
 *
 * @author Angel
 */

import java.util.ArrayList;

public interface dbPersistencia {
    //Insertar un nuevo usuario a la BD en la tabla usuario
    public void registrar(Object objeto) throws Exception;
    //Actualizar datos de un usuario
    public void actualizar(Object objecto) throws Exception;
     //Actualizar datos de un usuario
    public void puntuacion(Object objecto) throws Exception;
    //Extraer la pregunta de la BD
    public ArrayList generar(int nivel) throws Exception;
    //Lista el ranking del quiz
    public ArrayList listarUsuario() throws Exception;
    //Lista el ranking
    public ArrayList ranking() throws Exception;
    //Vaidar Existencia del  Usuario
    public boolean isExiste(String usuario) throws Exception;
    //Validar Contraseña
    public boolean isValidar(String Contraseña) throws Exception;
    //Validar usuario y contra
    public boolean isCoinciden(String usuario, String contra)throws Exception;
    //Devolver valores de los usuarios
    public Object buscar(String usuario)throws Exception;
}
