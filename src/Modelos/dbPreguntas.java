package Modelos;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Angel
 */
public class dbPreguntas extends dbManejador implements dbPersistencia {

        
    @Override
    public void registrar(Object objeto) throws Exception {
        Quiz quiz = new Quiz();
        quiz = (Quiz) objeto;
        String consulta = "";

        consulta = "INSERT INTO usuarios(usuario, contra, avatar, puntuacion) VALUES(?,?,?,?);";

        if (this.Conectar()) {
            try {
                System.err.println("se conecto");

                this.sqlConsulta = conexion.prepareStatement(consulta);
                //asignar valores a la consulta
                this.sqlConsulta.setString(1, quiz.getUsuario());
                this.sqlConsulta.setString(2, quiz.getContra());
                this.sqlConsulta.setString(3, Integer.toString(quiz.getAvatar()));
                this.sqlConsulta.setString(4, Integer.toString(quiz.getPuntuacion()));
                this.sqlConsulta.executeUpdate();
                this.Desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al insertar; " + e.getMessage());
            }
        }
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        Quiz quiz = new Quiz();
        quiz = (Quiz) objecto;

        String consulta = "UPDATE usuarios SET contra = ?  WHERE usuario = ?";

        if (this.Conectar()) {
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                //asignar valores a la consulta

                this.sqlConsulta.setString(1, quiz.getContra());
                this.sqlConsulta.setString(2, quiz.getUsuario());
                this.sqlConsulta.executeUpdate();

                this.Desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al Actualizar; " + e.getMessage());
            }
        }
    }

    @Override
    public ArrayList generar(int nivel) throws Exception {
        ArrayList<Preguntas> lista = new ArrayList<Preguntas>();
        Preguntas pregunta;

        if (this.Conectar()) {
            String consulta = "SELECT * FROM preguntas where nivel=?";
            
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setInt(1, nivel);
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            while (this.registros.next()) {
                pregunta = new Preguntas();
                pregunta.setId(this.registros.getInt("idPregunta"));
                pregunta.setPregunta(this.registros.getString("pregunta"));
                pregunta.setOp1(this.registros.getString("r1"));
                pregunta.setOp2(this.registros.getString("r2"));
                pregunta.setOp3(this.registros.getString("r3"));
                pregunta.setNivel(Integer.parseInt(this.registros.getString("nivel")));
                lista.add(pregunta);
            }
        }
        this.Desconectar();
        return lista;
    }

    @Override
    public ArrayList listarUsuario() throws Exception {
        ArrayList<Quiz> lista = new ArrayList<Quiz>();
        Quiz quiz;

        if (this.Conectar()) {
            String consulta = "SELECT * FROM usuarios ORDER BY puntuacion DESC LIMIT 3;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            while (this.registros.next()) {
                quiz = new Quiz();
                quiz.setId(this.registros.getInt("idUsuario"));
                quiz.setUsuario(this.registros.getString("usuario"));
                quiz.setContra(this.registros.getString("contra"));
                quiz.setAvatar(this.registros.getInt("avatar"));
                quiz.setPuntuacion(this.registros.getInt("puntuacion"));
                lista.add(quiz);
            }
        }
        this.Desconectar();
        return lista;
    }

    @Override
    public boolean isExiste(String usuario) throws Exception {
        boolean res = false;
        Quiz quiz = new Quiz();
        if (this.Conectar()) {
            String consulta = "SELECT * FROM usuarios WHERE usuario =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, usuario);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                res = true;
            }
        }
        this.Desconectar();
        return res;
    }

    @Override
    public boolean isValidar(String Contraseña) throws Exception {
        boolean res = false;
        Quiz quiz = new Quiz();
        if (this.Conectar()) {
            String consulta = "SELECT * FROM usuarios WHERE contra =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, quiz.getContra());
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                res = true;
            }
        }
        this.Desconectar();
        return res;
    }

    @Override
    public ArrayList ranking() throws Exception {
        ArrayList<Ranking> lista = new ArrayList<Ranking>();
        Ranking ranking;

        if (this.Conectar()) {
            String consulta = "SELECT * FROM ranking ORDER BY puntuacion DESC LIMIT 3;";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            //Sacar los registros
            while (this.registros.next()) {
                ranking = new Ranking();
                ranking.setIdRanking(this.registros.getInt("idRanking"));
                
                ranking.setUsuario(this.registros.getString("usuario"));
                ranking.setPuntuacion(this.registros.getInt("puntuacion")); 
                lista.add(ranking);
            }
        }
        this.Desconectar();
        return lista;
    }

    @Override
    public boolean isCoinciden(String usuario, String contra) throws Exception {
        boolean res = false;
        Quiz quiz = new Quiz();
        if (this.Conectar()) {
            String consulta = "SELECT * FROM usuarios WHERE usuario =? and contra =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, usuario);
            this.sqlConsulta.setString(2, contra);
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                res = true;
            }
        }
        this.Desconectar();
        return res;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(String usuario) throws Exception {
        Quiz quiz = new Quiz(); 
        if (this.Conectar()) {
            String consulta = "SELECT * FROM usuarios WHERE usuario =?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, usuario); 
            this.registros = this.sqlConsulta.executeQuery();
            if (this.registros.next()) {
                quiz.setUsuario(this.registros.getString("usuario"));
                quiz.setAvatar(this.registros.getInt("avatar")); 
                quiz.setPuntuacion(this.registros.getInt("puntuacion"));
            }
        }
        this.Desconectar(); 
        return quiz;
    //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void puntuacion(Object objecto) throws Exception {
        Quiz quiz = new Quiz();
        quiz = (Quiz) objecto;

        String consulta = "UPDATE usuarios SET puntuacion = ? WHERE usuario = ?";

        if (this.Conectar()) {
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                //asignar valores a la consulta

                this.sqlConsulta.setString(1, Integer.toString(quiz.getPuntuacion()));
                this.sqlConsulta.setString(2, quiz.getUsuario());
                this.sqlConsulta.executeUpdate();

                this.Desconectar();
            } catch (SQLException e) {
                System.err.println("Surgio un error al Actualizar el puntaje " + e.getMessage());
            }
        }
    }

}
